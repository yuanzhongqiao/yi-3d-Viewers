<div class="Box-sc-g0xbh4-0 bJMeLZ js-snippet-clipboard-copy-unpositioned" data-hpc="true"><article class="markdown-body entry-content container-lg" itemprop="text">

<div align="center" dir="auto">
  <div class="markdown-heading" dir="auto"><h1 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">OHIF 医学影像查看器</font></font></h1><a id="user-content-ohif-medical-imaging-viewer" class="anchor" aria-label="永久链接：OHIF 医学影像查看器" href="#ohif-medical-imaging-viewer"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
  <p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">OHIF Viewer是</font></font></strong><font style="vertical-align: inherit;"></font><a href="https://ohif.org/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">开放健康影像基金会 (OHIF)</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">提供的零占用空间医学图像查看器。它是一个可配置且可扩展的渐进式 Web 应用程序，具有对支持</font></font><a href="https://www.dicomstandard.org/using/dicomweb/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">DICOMweb 的</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">图像档案的开箱即用支持</font><font style="vertical-align: inherit;">。</font></font></p>
</div>
<div align="center" dir="auto">
  <a href="https://docs.ohif.org/" rel="nofollow"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">阅读文档</font></font></strong></a>
</div>
<div align="center" dir="auto">
  <a href="https://viewer.ohif.org/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">现场演示</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">|
  </font></font><a href="https://ui.ohif.org/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">组件库</font></font></a>
</div>
<div align="center" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
  📰</font></font><a href="https://ohif.org/news/" rel="nofollow"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">订阅 OHIF 新闻通讯</font></font></strong></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">📰
</font></font></div>
<div align="center" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
  📰</font></font><a href="https://ohif.org/news/" rel="nofollow"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">订阅 OHIF 新闻通讯</font></font></strong></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">📰
</font></font></div>
<hr>
<p dir="auto"><a href="https://npmjs.org/package/@ohif/app" rel="nofollow"><img src="https://camo.githubusercontent.com/928b293a63678da1fce352109ceb6b18f37672f65c285e932b78423c1e8ffd76/68747470733a2f2f696d672e736869656c64732e696f2f6e706d2f762f406f6869662f6170702e7376673f7374796c653d666c61742d737175617265" alt="NPM 版本" data-canonical-src="https://img.shields.io/npm/v/@ohif/app.svg?style=flat-square" style="max-width: 100%;"></a>
<a href="/OHIF/Viewers/blob/master/LICENSE"><img src="https://camo.githubusercontent.com/e51b657236415672754f02dfef0bc6873e979346fa0107f9c4219fe1589a5c6c/68747470733a2f2f696d672e736869656c64732e696f2f62616467652f6c6963656e73652d4d49542d626c75652e7376673f7374796c653d666c61742d737175617265" alt="MIT 许可证" data-canonical-src="https://img.shields.io/badge/license-MIT-blue.svg?style=flat-square" style="max-width: 100%;"></a>
<a href="/OHIF/Viewers/blob/master/percy-url"><img src="https://camo.githubusercontent.com/6e34399ce47e4dfafb8fe167d20cb55e37835c136651f93f371d0c6c9a860db7/68747470733a2f2f70657263792e696f2f7374617469632f696d616765732f70657263792d62616467652e737667" alt="该项目正在使用 Percy.io 进行视觉回归测试。" data-canonical-src="https://percy.io/static/images/percy-badge.svg" style="max-width: 100%;"></a></p>








<table>
<thead>
<tr>
<th align="center"></th>
<th align="left"></th>
<th align="left"></th>
</tr>
</thead>
<tbody>
<tr>
<td align="center"><a target="_blank" rel="noopener noreferrer" href="https://github.com/OHIF/Viewers/blob/master/platform/docs/docs/assets/img/demo-measurements.webp?raw=true"><img src="https://github.com/OHIF/Viewers/raw/master/platform/docs/docs/assets/img/demo-measurements.webp?raw=true" alt="测量追踪" width="350" style="max-width: 100%;"></a></td>
<td align="left"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">测量追踪</font></font></td>
<td align="left"><a href="https://viewer.ohif.org/viewer?StudyInstanceUIDs=1.3.6.1.4.1.25403.345050719074.3824.20170125095438.5" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">演示</font></font></a></td>
</tr>
<tr>
<td align="center"><a target="_blank" rel="noopener noreferrer" href="https://github.com/OHIF/Viewers/blob/master/platform/docs/docs/assets/img/demo-segmentation.webp?raw=true"><img src="https://github.com/OHIF/Viewers/raw/master/platform/docs/docs/assets/img/demo-segmentation.webp?raw=true" alt="细分" width="350" style="max-width: 100%;"></a></td>
<td align="left"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">标签图分割</font></font></td>
<td align="left"><a href="https://viewer.ohif.org/viewer?StudyInstanceUIDs=1.3.12.2.1107.5.2.32.35162.30000015050317233592200000046" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">演示</font></font></a></td>
</tr>
<tr>
<td align="center"><a target="_blank" rel="noopener noreferrer" href="https://github.com/OHIF/Viewers/blob/master/platform/docs/docs/assets/img/demo-ptct.webp?raw=true"><img src="https://github.com/OHIF/Viewers/raw/master/platform/docs/docs/assets/img/demo-ptct.webp?raw=true" alt="悬挂协议" width="350" style="max-width: 100%;"></a></td>
<td align="left"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">融合和定制悬挂协议</font></font></td>
<td align="left"><a href="https://viewer.ohif.org/tmtv?StudyInstanceUIDs=1.3.6.1.4.1.14519.5.2.1.7009.2403.334240657131972136850343327463" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">演示</font></font></a></td>
</tr>
<tr>
<td align="center"><a target="_blank" rel="noopener noreferrer" href="https://github.com/OHIF/Viewers/blob/master/platform/docs/docs/assets/img/demo-volume-rendering.webp?raw=true"><img src="https://github.com/OHIF/Viewers/raw/master/platform/docs/docs/assets/img/demo-volume-rendering.webp?raw=true" alt="体积渲染" width="350" style="max-width: 100%;"></a></td>
<td align="left"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">体积渲染</font></font></td>
<td align="left"><a href="https://viewer.ohif.org/viewer?StudyInstanceUIDs=1.3.6.1.4.1.25403.345050719074.3824.20170125095438.5&amp;hangingprotocolId=mprAnd3DVolumeViewport" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">演示</font></font></a></td>
</tr>
<tr>
<td align="center"><a target="_blank" rel="noopener noreferrer" href="https://github.com/OHIF/Viewers/blob/master/platform/docs/docs/assets/img/demo-pdf.webp?raw=true"><img src="https://github.com/OHIF/Viewers/raw/master/platform/docs/docs/assets/img/demo-pdf.webp?raw=true" alt="PDF" width="350" style="max-width: 100%;"></a></td>
<td align="left"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">PDF</font></font></td>
<td align="left"><a href="https://viewer.ohif.org/viewer?StudyInstanceUIDs=2.25.317377619501274872606137091638706705333" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">演示</font></font></a></td>
</tr>
<tr>
<td align="center"><a target="_blank" rel="noopener noreferrer" href="https://github.com/OHIF/Viewers/blob/master/platform/docs/docs/assets/img/demo-rtstruct.webp?raw=true"><img src="https://github.com/OHIF/Viewers/raw/master/platform/docs/docs/assets/img/demo-rtstruct.webp?raw=true" alt="结构体" width="350" style="max-width: 100%;"></a></td>
<td align="left"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">RT 结构</font></font></td>
<td align="left"><a href="https://viewer.ohif.org/viewer?StudyInstanceUIDs=1.3.6.1.4.1.5962.99.1.2968617883.1314880426.1493322302363.3.0" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">演示</font></font></a></td>
</tr>
<tr>
<td align="center"><a target="_blank" rel="noopener noreferrer" href="https://github.com/OHIF/Viewers/blob/master/platform/docs/docs/assets/img/demo-4d.webp?raw=true"><img src="https://github.com/OHIF/Viewers/raw/master/platform/docs/docs/assets/img/demo-4d.webp?raw=true" alt="4D" width="350" style="max-width: 100%;"></a></td>
<td align="left"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">4D</font></font></td>
<td align="left"><a href="https://viewer.ohif.org/dynamic-volume?StudyInstanceUIDs=2.25.232704420736447710317909004159492840763" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">演示</font></font></a></td>
</tr>
<tr>
<td align="center"><a target="_blank" rel="noopener noreferrer" href="https://github.com/OHIF/Viewers/blob/master/platform/docs/docs/assets/img/demo-video.webp?raw=true"><img src="https://github.com/OHIF/Viewers/raw/master/platform/docs/docs/assets/img/demo-video.webp?raw=true" alt="视频" width="350" style="max-width: 100%;"></a></td>
<td align="left"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">视频</font></font></td>
<td align="left"><a href="https://viewer.ohif.org/viewer?StudyInstanceUIDs=2.25.96975534054447904995905761963464388233" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">演示</font></font></a></td>
</tr>
<tr>
<td align="center"><a target="_blank" rel="noopener noreferrer" href="https://github.com/OHIF/Viewers/blob/master/platform/docs/docs/assets/img/microscopy.webp?raw=true"><img src="https://github.com/OHIF/Viewers/raw/master/platform/docs/docs/assets/img/microscopy.webp?raw=true" alt="显微镜" width="350" style="max-width: 100%;"></a></td>
<td align="left"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">载玻片显微镜</font></font></td>
<td align="left"><a href="https://viewer.ohif.org/microscopy?StudyInstanceUIDs=2.25.141277760791347900862109212450152067508" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">演示</font></font></a></td>
</tr>
</tbody>
</table>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">关于</font></font></h2><a id="user-content-about" class="anchor" aria-label="固定链接：关于" href="#about"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">OHIF Viewer 可以从大多数来源和格式检索和加载图像；以 2D、3D 和重建表示形式渲染集合；允许对观察结果进行操作、注释和序列化；支持国际化、OpenID Connect、离线使用、热键等更多功能。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">几乎所有东西都提供一定程度的定制和配置。如果它不支持您需要的某些功能，我们会接受拉取请求，并不断改进扩展系统。</font></font></p>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">为什么选择我们</font></font></h2><a id="user-content-why-choose-us" class="anchor" aria-label="永久链接：为什么选择我们" href="#why-choose-us"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">社区与体验</font></font></h3><a id="user-content-community--experience" class="anchor" aria-label="固定链接：社区与经验" href="#community--experience"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">OHIF Viewer 是一项协作成果，已成为许多活跃、生产和 FDA 批准的医学影像查看器的基础。它受益于我们广泛的社区集体经验，以及个人、研究团体和商业组织的赞助贡献。</font></font></p>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">为适应而生</font></font></h3><a id="user-content-built-to-adapt" class="anchor" aria-label="固定链接：为适应而生" href="#built-to-adapt"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">经过 8 年多与众多公司和组织的整合，OHIF Viewer 已从头开始重建，以更好地满足众多用户不同的工作流程和配置需求。Viewer 的所有核心功能均使用其自己的扩展系统构建。同样的可扩展性使我们能够提供：</font></font></p>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">2D 和 3D 医学图像查看</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">多平面重建 (MPR)</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">最大强度项目 (MIP)</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">全载玻片显微镜观察</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">PDF 和 Dicom 结构化报告渲染</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">分割渲染为标签图和轮廓</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">用户访问控制 (UAC)</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">上下文特定的工具栏和侧面板内容</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">以及其他许多人</font></font></li>
</ul>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">您可以利用它为您的工作流程定制查看器，并添加您可能需要的任何新功能（并且希望私下维护而不分叉）。</font></font></p>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">支持</font></font></h3><a id="user-content-support" class="anchor" aria-label="固定链接：支持" href="#support"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<ul dir="auto">
<li><a href="https://github.com/OHIF/Viewers/issues/new?assignees=&amp;labels=Community%3A+Report+%3Abug%3A%2CAwaiting+Reproduction&amp;projects=&amp;template=bug-report.yml&amp;title=%5BBug%5D+"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">报告错误🐛</font></font></a></li>
<li><a href="https://github.com/OHIF/Viewers/issues/new?assignees=&amp;labels=Community%3A+Request+%3Ahand%3A&amp;projects=&amp;template=feature-request.yml&amp;title=%5BFeature+Request%5D+"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">请求功能🚀</font></font></a></li>
<li><a href="/OHIF/Viewers/blob/master/community.ohif.org"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">提问 🤗</font></font></a></li>
<li><a href="https://join.slack.com/t/cornerstonejs/shared_invite/zt-1r8xb2zau-dOxlD6jit3TN0Uwf928w9Q" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Slack 频道</font></font></a></li>
</ul>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">如需商业支持、学术合作和常见问题的解答，请使用</font></font><a href="https://ohif.org/get-support/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">获取支持</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">与我们联系。</font></font></p>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">发展</font></font></h2><a id="user-content-developing" class="anchor" aria-label="永久链接：开发中" href="#developing"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">分支</font></font></h3><a id="user-content-branches" class="anchor" aria-label="永久链接：分支" href="#branches"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<div class="markdown-heading" dir="auto"><h4 tabindex="-1" class="heading-element" dir="auto"><code>master</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">分支 - 最新的开发（测试）版本</font></font></h4><a id="user-content-master-branch---the-latest-dev-beta-release" class="anchor" aria-label="永久链接：master 分支 - 最新开发（测试版）版本" href="#master-branch---the-latest-dev-beta-release"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<ul dir="auto">
<li><code>master</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">- 最新开发版本</font></font></li>
</ul>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">这通常是最新开发发生的地方。主分支中的代码已通过代码审查和自动化测试，但可能尚未被视为可用于生产。此分支通常包含开发团队正在开发的最新更改和功能。它通常是创建功能分支（开发新功能）和修补程序分支（用于紧急修复）的起点。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">每个包都标有 beta 版本号，并发布到 npm，例如</font></font><code>@ohif/ui@3.6.0-beta.1</code></p>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><code>release/*</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">分支 - 最新稳定版本</font></font></h3><a id="user-content-release-branches---the-latest-stable-releases" class="anchor" aria-label="永久链接：release/* 分支 - 最新稳定版本" href="#release-branches---the-latest-stable-releases"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">一旦</font></font><code>master</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">分支代码达到稳定、可发布的状态，我们就会进行全面的代码审查和 QA 测试。经批准后，我们&ZeroWidthSpace;&ZeroWidthSpace;会从中创建一个新的发布分支</font></font><code>master</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">。这些分支代表被认为可用于生产的最新稳定版本。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">例如，</font></font><code>release/3.5</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">是版本 3.5.0 的分支，</font></font><code>release/3.6</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">是版本 3.6.0 的分支。每次发布后，我们都会等待几天以确保没有严重错误。如果发现任何错误，我们会在发布分支中修复它们，并在分支中创建一个具有次要版本升级的新版本，例如 3.5.1 </font></font><code>release/3.5</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">每个包都标有版本号并发布到 npm，例如</font></font><code>@ohif/ui@3.5.0</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">。请注意，</font></font><code>master</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">始终领先于</font></font><code>release</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">分支。我们发布了 beta 版和稳定版的 docker 版本。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">以下是我们开发工作流程的示意图：</font></font></p>
<p dir="auto"><a target="_blank" rel="noopener noreferrer" href="/OHIF/Viewers/blob/master/platform/docs/docs/assets/img/github-readme-branches-Jun2024.png"><img src="/OHIF/Viewers/raw/master/platform/docs/docs/assets/img/github-readme-branches-Jun2024.png" alt="替代文本" style="max-width: 100%;"></a></p>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">要求</font></font></h3><a id="user-content-requirements" class="anchor" aria-label="固定链接：要求" href="#requirements"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<ul dir="auto">
<li><a href="https://yarnpkg.com/en/docs/install" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Yarn 1.17.3+</font></font></a></li>
<li><a href="https://nodejs.org/en/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">节点 18+</font></font></a></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">你的机器上应该启用 Yarn Workspaces：
</font></font><ul dir="auto">
<li><code>yarn config set workspaces-experimental true</code></li>
</ul>
</li>
</ul>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">入门</font></font></h3><a id="user-content-getting-started" class="anchor" aria-label="永久链接：入门" href="#getting-started"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<ol dir="auto">
<li><a href="https://help.github.com/en/articles/fork-a-repo"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Fork此存储库</font></font></a></li>
<li><a href="https://help.github.com/en/articles/fork-a-repo#step-2-create-a-local-clone-of-your-fork"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">克隆你的分支仓库</font></font></a>
<ul dir="auto">
<li><code>git clone https://github.com/YOUR-USERNAME/Viewers.git</code></li>
</ul>
</li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">导航到克隆项目的目录</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">将此 repo 添加为</font></font><code>remote</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">命名</font></font><code>upstream</code>
<ul dir="auto">
<li><code>git remote add upstream https://github.com/OHIF/Viewers.git</code></li>
</ul>
</li>
<li><code>yarn install</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">恢复依赖关系并链接项目</font></font></li>
</ol>
<div class="markdown-heading" dir="auto"><h4 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">发展</font></font></h4><a id="user-content-to-develop" class="anchor" aria-label="永久链接：开发" href="#to-develop"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><em><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">从该存储库的根目录：</font></font></em></p>
<div class="highlight highlight-source-shell notranslate position-relative overflow-auto" dir="auto"><pre><span class="pl-c"><span class="pl-c">#</span> Enable Yarn Workspaces</span>
yarn config <span class="pl-c1">set</span> workspaces-experimental <span class="pl-c1">true</span>

<span class="pl-c"><span class="pl-c">#</span> Restore dependencies</span>
yarn install</pre><div class="zeroclipboard-container">
    <clipboard-copy aria-label="Copy" class="ClipboardButton btn btn-invisible js-clipboard-copy m-2 p-0 tooltipped-no-delay d-flex flex-justify-center flex-items-center" data-copy-feedback="Copied!" data-tooltip-direction="w" value="# Enable Yarn Workspaces
yarn config set workspaces-experimental true

# Restore dependencies
yarn install" tabindex="0" role="button">
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-copy js-clipboard-copy-icon">
    <path d="M0 6.75C0 5.784.784 5 1.75 5h1.5a.75.75 0 0 1 0 1.5h-1.5a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-1.5a.75.75 0 0 1 1.5 0v1.5A1.75 1.75 0 0 1 9.25 16h-7.5A1.75 1.75 0 0 1 0 14.25Z"></path><path d="M5 1.75C5 .784 5.784 0 6.75 0h7.5C15.216 0 16 .784 16 1.75v7.5A1.75 1.75 0 0 1 14.25 11h-7.5A1.75 1.75 0 0 1 5 9.25Zm1.75-.25a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-7.5a.25.25 0 0 0-.25-.25Z"></path>
</svg>
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-check js-clipboard-check-icon color-fg-success d-none">
    <path d="M13.78 4.22a.75.75 0 0 1 0 1.06l-7.25 7.25a.75.75 0 0 1-1.06 0L2.22 9.28a.751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018L6 10.94l6.72-6.72a.75.75 0 0 1 1.06 0Z"></path>
</svg>
    </clipboard-copy>
  </div></div>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">命令</font></font></h2><a id="user-content-commands" class="anchor" aria-label="永久链接：命令" href="#commands"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">这些命令可从根目录获得。每个项目目录还支持许多命令，可在各自的
</font></font><code>README.md</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">和</font></font><code>package.json</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">文件中找到。</font></font></p>
<table>
<thead>
<tr>
<th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Yarn 命令</font></font></th>
<th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">描述</font></font></th>
</tr>
</thead>
<tbody>
<tr>
<td><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">发展</font></font></strong></td>
<td></td>
</tr>
<tr>
<td><code>dev</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">或者</font></font><code>start</code></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Viewer 的默认开发体验</font></font></td>
</tr>
<tr>
<td><code>test:unit</code></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Jest 多项目测试运行器；全面覆盖</font></font></td>
</tr>
<tr>
<td><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">部署</font></font></strong></td>
<td></td>
</tr>
<tr>
<td><code>build</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">*</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">为我们的 PWA Viewer 构建生产输出</font></font></td>
</tr>
</tbody>
</table>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">* - 有关我们不同版本的更多信息，请查看我们的</font></font><a href="https://docs.ohif.org/deployment/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">部署文档</font></font></a></p>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">项目</font></font></h2><a id="user-content-project" class="anchor" aria-label="永久链接：项目" href="#project"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">OHIF 医学图像查看平台以 的形式进行维护
</font></font><a href="https://en.wikipedia.org/wiki/Monorepo" rel="nofollow"><code>monorepo</code></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">。这意味着此存储库不包含单个项目，而是包含许多项目。如果您探索我们的项目结构，您将看到以下内容：</font></font></p>
<div class="highlight highlight-source-shell notranslate position-relative overflow-auto" dir="auto"><pre><span class="pl-c1">.</span>
├── extensions               <span class="pl-c"><span class="pl-c">#</span></span>
│   ├── _example             <span class="pl-c"><span class="pl-c">#</span> Skeleton of example extension</span>
│   ├── default              <span class="pl-c"><span class="pl-c">#</span> basic set of useful functionalities (datasources, panels, etc)</span>
│   ├── cornerstone       <span class="pl-c"><span class="pl-c">#</span> image rendering and tools w/ Cornerstone3D</span>
│   ├── cornerstone-dicom-sr <span class="pl-c"><span class="pl-c">#</span> DICOM Structured Report rendering and export</span>
│   ├── cornerstone-dicom-sr <span class="pl-c"><span class="pl-c">#</span> DICOM Structured Report rendering and export</span>
│   ├── cornerstone-dicom-seg <span class="pl-c"><span class="pl-c">#</span> DICOM Segmentation rendering and export</span>
│   ├── cornerstone-dicom-rt <span class="pl-c"><span class="pl-c">#</span> DICOM RTSTRUCT rendering</span>
│   ├── cornerstone-microscopy <span class="pl-c"><span class="pl-c">#</span> Whole Slide Microscopy rendering</span>
│   ├── dicom-pdf <span class="pl-c"><span class="pl-c">#</span> PDF rendering</span>
│   ├── dicom-video <span class="pl-c"><span class="pl-c">#</span> DICOM RESTful Services</span>
│   ├── measurement-tracking <span class="pl-c"><span class="pl-c">#</span> Longitudinal measurement tracking</span>
│   ├── tmtv <span class="pl-c"><span class="pl-c">#</span> Total Metabolic Tumor Volume (TMTV) calculation</span>
<span class="pl-k">|</span>

│
├── modes                    <span class="pl-c"><span class="pl-c">#</span></span>
│   ├── _example             <span class="pl-c"><span class="pl-c">#</span> Skeleton of example mode</span>
│   ├── basic-dev-mode       <span class="pl-c"><span class="pl-c">#</span> Basic development mode</span>
│   ├── longitudinal         <span class="pl-c"><span class="pl-c">#</span> Longitudinal mode (measurement tracking)</span>
│   ├── tmtv       <span class="pl-c"><span class="pl-c">#</span> Total Metabolic Tumor Volume (TMTV) calculation mode</span>
│   └── microscopy          <span class="pl-c"><span class="pl-c">#</span> Whole Slide Microscopy mode</span>
│
├── platform                 <span class="pl-c"><span class="pl-c">#</span></span>
│   ├── core                 <span class="pl-c"><span class="pl-c">#</span> Business Logic</span>
│   ├── i18n                 <span class="pl-c"><span class="pl-c">#</span> Internationalization Support</span>
│   ├── ui                   <span class="pl-c"><span class="pl-c">#</span> React component library</span>
│   ├── docs                 <span class="pl-c"><span class="pl-c">#</span> Documentation</span>
│   └── viewer               <span class="pl-c"><span class="pl-c">#</span> Connects platform and extension projects</span>
│
├── ...                      <span class="pl-c"><span class="pl-c">#</span> misc. shared configuration</span>
├── lerna.json               <span class="pl-c"><span class="pl-c">#</span> MonoRepo (Lerna) settings</span>
├── package.json             <span class="pl-c"><span class="pl-c">#</span> Shared devDependencies and commands</span>
└── README.md                <span class="pl-c"><span class="pl-c">#</span> This file</span></pre><div class="zeroclipboard-container">
    <clipboard-copy aria-label="Copy" class="ClipboardButton btn btn-invisible js-clipboard-copy m-2 p-0 tooltipped-no-delay d-flex flex-justify-center flex-items-center" data-copy-feedback="Copied!" data-tooltip-direction="w" value=".
├── extensions               #
│   ├── _example             # Skeleton of example extension
│   ├── default              # basic set of useful functionalities (datasources, panels, etc)
│   ├── cornerstone       # image rendering and tools w/ Cornerstone3D
│   ├── cornerstone-dicom-sr # DICOM Structured Report rendering and export
│   ├── cornerstone-dicom-sr # DICOM Structured Report rendering and export
│   ├── cornerstone-dicom-seg # DICOM Segmentation rendering and export
│   ├── cornerstone-dicom-rt # DICOM RTSTRUCT rendering
│   ├── cornerstone-microscopy # Whole Slide Microscopy rendering
│   ├── dicom-pdf # PDF rendering
│   ├── dicom-video # DICOM RESTful Services
│   ├── measurement-tracking # Longitudinal measurement tracking
│   ├── tmtv # Total Metabolic Tumor Volume (TMTV) calculation
|

│
├── modes                    #
│   ├── _example             # Skeleton of example mode
│   ├── basic-dev-mode       # Basic development mode
│   ├── longitudinal         # Longitudinal mode (measurement tracking)
│   ├── tmtv       # Total Metabolic Tumor Volume (TMTV) calculation mode
│   └── microscopy          # Whole Slide Microscopy mode
│
├── platform                 #
│   ├── core                 # Business Logic
│   ├── i18n                 # Internationalization Support
│   ├── ui                   # React component library
│   ├── docs                 # Documentation
│   └── viewer               # Connects platform and extension projects
│
├── ...                      # misc. shared configuration
├── lerna.json               # MonoRepo (Lerna) settings
├── package.json             # Shared devDependencies and commands
└── README.md                # This file" tabindex="0" role="button">
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-copy js-clipboard-copy-icon">
    <path d="M0 6.75C0 5.784.784 5 1.75 5h1.5a.75.75 0 0 1 0 1.5h-1.5a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-1.5a.75.75 0 0 1 1.5 0v1.5A1.75 1.75 0 0 1 9.25 16h-7.5A1.75 1.75 0 0 1 0 14.25Z"></path><path d="M5 1.75C5 .784 5.784 0 6.75 0h7.5C15.216 0 16 .784 16 1.75v7.5A1.75 1.75 0 0 1 14.25 11h-7.5A1.75 1.75 0 0 1 5 9.25Zm1.75-.25a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-7.5a.25.25 0 0 0-.25-.25Z"></path>
</svg>
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-check js-clipboard-check-icon color-fg-success d-none">
    <path d="M13.78 4.22a.75.75 0 0 1 0 1.06l-7.25 7.25a.75.75 0 0 1-1.06 0L2.22 9.28a.751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018L6 10.94l6.72-6.72a.75.75 0 0 1 1.06 0Z"></path>
</svg>
    </clipboard-copy>
  </div></div>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">致谢</font></font></h2><a id="user-content-acknowledgments" class="anchor" aria-label="永久链接：致谢" href="#acknowledgments"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">若要在学术出版物中提及 OHIF Viewer，请引用</font></font></p>
<blockquote>
<p dir="auto"><em><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">开放健康影像基金会查看器：一个可扩展的开源框架，用于构建基于 Web 的影像应用程序以支持癌症研究</font></font></em></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Erik Ziegler、Trinity Urban、Danny Brown、James Petts、Steve D. Pieper、Rob Lewis、Chris Hafey 和 Gordon J. Harris</font></font></p>
<p dir="auto"><em><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">JCO 临床癌症信息学</font></font></em><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">，第 4 期 (2020)，336-345，DOI：
 </font></font><a href="https://www.doi.org/10.1200/CCI.19.00131" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">10.1200/CCI.19.00131</font></font></a></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Pubmed Central 上的开放存取：
 </font></font><a href="https://www.ncbi.nlm.nih.gov/pmc/articles/PMC7259879/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://www.ncbi.nlm.nih.gov/pmc/articles/PMC7259879/</font></font></a></p>
</blockquote>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">或者，对于 v1，请引用：</font></font></p>
<blockquote>
<p dir="auto"><em><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">LesionTracker：用于癌症成像研究和临床试验的可扩展开源零占用空间 Web 查看器</font></font></em></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Trinity Urban、Erik Ziegler、Rob Lewis、Chris Hafey、Cheryl Sadow、Annick D. Van den Abbeele 和 Gordon J. Harris</font></font></p>
<p dir="auto"><em><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">癌症研究</font></font></em><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">，2017 年 11 月 1 日 (77) (21) e119-e122 DOI:
 </font></font><a href="https://www.doi.org/10.1158/0008-5472.CAN-17-0334" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">10.1158/0008-5472.CAN-17-0334</font></font></a></p>
</blockquote>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">注意：</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">如果您使用或发现此存储库有用，请花点时间在 GitHub 上为该存储库加注星标。这是我们评估采用情况的一种简单方法，它可以帮助我们为该项目获得未来的资金。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">这项工作主要由美国国立卫生研究院、美国国家癌症研究所、癌症研究信息技术 (ITCR) 计划资助，由
</font></font><a href="https://projectreporter.nih.gov/project_info_description.cfm?aid=8971104" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">马萨诸塞州总医院的 Gordon Harris 博士 (U24 CA199460) 资助</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">。</font></font></p>
<p dir="auto"><a href="https://imaging.datacommons.cancer.gov/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">NCI 影像数据共享 (IDC) 项目</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">支持开发新功能和错误修复，这些功能和错误修复标有</font></font><a href="https://github.com/OHIF/Viewers/issues?q=is%3Aissue+is%3Aopen+label%3AIDC%3Apriority"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">“IDC:priority”</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">、
 </font></font><a href="https://github.com/OHIF/Viewers/issues?q=is%3Aissue+is%3Aopen+label%3AIDC%3Acandidate"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">“IDC:candidate”</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">或</font></font><a href="https://github.com/OHIF/Viewers/issues?q=is%3Aissue+is%3Aopen+label%3AIDC%3Acollaboration"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">“IDC:collaboration”</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">。NCI 影像数据共享由 Leidos Biomedical Research 的合同号 19X037Q 支持，合同号为 NCI 的任务订单 HHSN26100071。IDC </font></font><a href="https://learn.canceridc.dev/portal/visualization" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Viewer</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">是 OHIF Viewer 的定制版本。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">本项目使用 BrowserStack 测试。感谢您支持开源！</font></font></p>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">执照</font></font></h2><a id="user-content-license" class="anchor" aria-label="永久链接：许可证" href="#license"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">麻省理工学院© </font></font><a href="https://github.com/OHIF"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">OHIF</font></font></a></p>








<p dir="auto"><a href="https://app.fossa.io/projects/git%2Bgithub.com%2FOHIF%2FViewers?ref=badge_large" rel="nofollow"><img src="https://camo.githubusercontent.com/f7f2d5517fde8ec81f4da7383710aa32a50573ced7ad99133036f9ec47b0e193/68747470733a2f2f6170702e666f7373612e696f2f6170692f70726f6a656374732f6769742532426769746875622e636f6d2532464f484946253246566965776572732e7376673f747970653d6c61726765" alt="FOSSA 状态" data-canonical-src="https://app.fossa.io/api/projects/git%2Bgithub.com%2FOHIF%2FViewers.svg?type=large" style="max-width: 100%;"></a></p>
</article></div>
